import { Movie } from "./movie.model";

export class Actor{
    actorId?: number;
    name?: string;
    country?: string;
    birthday?: string;
    movie?: Movie;
}