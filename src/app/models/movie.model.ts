import { Actor } from "./actor.model";

export class Movie {
    movieId?: number;
    title?: string;
    year?: string;
    duration?: string;
    actorList?: Actor[];
}