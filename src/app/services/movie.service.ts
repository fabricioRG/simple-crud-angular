import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Movie } from '../models/movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  getMoviesByTitleURL = "/back-test/rest/movies";
  getMovieByIdURL = "/back-test/rest/movies/";
  createMovieURL = "/back-test/rest/movies";
  updateMovieURL = "/back-test/rest/movies";
  deleteMovieURL = "/back-test/rest/movies/";

  constructor(private _http: HttpClient) { }

  getMoviesByTitle(movieTitle: string) {
    return this._http.get<Movie[]>(
      this.getMoviesByTitleURL,
      {
        params: {
          title: movieTitle
        }
      }
    )
  }

  getMovieById(movieId: number){
    let finalURL = this.getMovieByIdURL + movieId;
    return this._http.get<Movie>(
      finalURL
    )
  }

  createMovie(movie: Movie){
    return this._http.put<any>(
      this.createMovieURL,
      movie
    )
  }

  updateMovie(movie: Movie){
    return this._http.post<any>(
      this.updateMovieURL,
      movie
    )
  }

  deleteMovie(movieId: number){
    let finalURL = this.deleteMovieURL + movieId;
    return this._http.delete<any>(
      finalURL
    )
  }

}
