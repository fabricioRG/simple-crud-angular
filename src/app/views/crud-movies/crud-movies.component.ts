import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-crud-movies',
  templateUrl: './crud-movies.component.html',
  styleUrls: ['./crud-movies.component.css'],
})
export class CrudMoviesComponent implements OnInit {
  
  movies: Movie[] = [];
  displayedMovieColumns: string[] = ['movieId', 'title', 'year', 'duration','update','delete'];
  movieTitleValue = '';
  movieYearValue = '';
  movieDurationValue = '';
  isMovieSelected = false;
  selectedMovie: Movie = {};

  constructor(private movieService: MovieService) {}

  ngOnInit(): void {
    this.getMoviesByTitle('');
  }

  getMoviesByTitle(title: string) {
    this.movieService.getMoviesByTitle(title).subscribe((response) => {
      this.movies = response;
      console.log(this.movies);
    });
  }

  getMoviesById(movieId: number) {
    this.movieService.getMovieById(movieId).subscribe((response) => {
      this.movies = [];
      this.movies.push(response);
    });
  }

  createMovie(movie: Movie) {
    this.movieService.createMovie(movie).subscribe(() => {
      this.getMoviesByTitle('');
    });
  }

  deleteMovie(movie: Movie){
    if(movie != undefined){
      if(movie.movieId != undefined){
        this.movieService.deleteMovie(movie.movieId)
          .subscribe(() => {
            this.getMoviesByTitle('');
          })
      }
    }
  }

  applyFilterByName(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.getMoviesByTitle(filterValue);
  }

  applyFilterById(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (filterValue == undefined) {
      this.getMoviesById(0);
    }
    this.getMoviesById(Number(filterValue));
  }
  
    updateMovieItem(movie: Movie){
      if(movie != undefined){
        this.isMovieSelected = true;
        this.selectedMovie = movie;
        this.movieTitleValue = movie.title!;
        this.movieYearValue = movie.year!;
        this.movieDurationValue = movie.duration!;
      }
    }

  updateMovie(movie: Movie){
    if(movie != undefined){
      if(movie != undefined){
        movie.title = this.movieTitleValue;
        movie.year = this.movieYearValue;
        movie.duration = this.movieDurationValue;
        this.movieService.updateMovie(movie)
          .subscribe(() => {
            this.getMoviesByTitle('');
            this.setDefaultValues();
            this.isMovieSelected = false;
          })
      }
    }
  }

  saveMovie() {
    if (this.isMovieSelected) {
      this.updateMovie(this.selectedMovie);
    } else {
      let newMovie: Movie = {
        title: this.movieTitleValue,
        year: this.movieYearValue,
        duration: this.movieDurationValue,
      };
      this.createMovie(newMovie);
    }
    this.setDefaultValues();
  }

  cancel(){
    this.setDefaultValues();
    this.isMovieSelected = false;
  }

  isBlank(str: string) {
    return !str.trim() || str.length === 0;
  }

  setDefaultValues(){
    this.movieTitleValue = '';
    this.movieYearValue = '';
    this.movieDurationValue = '';
  }

}
