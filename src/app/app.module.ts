import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CrudMoviesComponent } from './views/crud-movies/crud-movies.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button'; 
import {MatIconModule} from '@angular/material/icon';



import { HttpClient, HttpParams} from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


const rutas: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'crudMovies'
  },
  {
    path: 'crudMovies',
    component: CrudMoviesComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    CrudMoviesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(rutas, {
      useHash: true
    }),
    BrowserAnimationsModule,
    MatTableModule,
    HttpClientModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
