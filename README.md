# SimpleCrud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.9.

## Development server

Run `ng serve --proxy-config proxy.conf.json` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Movies CRUD 

Actions in object Movie
* Create
* Find
* Update
* Delete

### Create Movie

Fill required fields

![Alt text](src/assets/imgs/create-1.png?raw=true "Create movie")

Press button "Save"

![Alt text](src/assets/imgs/create-2.png?raw=true "Create movie")

### Find Movie

Find by Title

![Alt text](src/assets/imgs/find-by-1.png?raw=true "Create movie")

Find by Id

![Alt text](src/assets/imgs/find-by-2.png?raw=true "Create movie")

### Update Movie

Selected movie

![Alt text](src/assets/imgs/update-1.png?raw=true "Create movie")

Press button "Save"

![Alt text](src/assets/imgs/update-2.png?raw=true "Create movie")
